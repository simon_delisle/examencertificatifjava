package com.example.examencertificatifsimondelisle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class SignIn extends AppCompatActivity {

    private Button button_enregistrer;
    private TextView textView_signIn;
    private EditText editText_utilisateur, editText_mot, editText_confirmez;
    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        button_enregistrer = (Button) findViewById(R.id.button_enregistrer);

        textView_signIn = (TextView) findViewById(R.id.textView_signIn);
        editText_confirmez = (EditText) findViewById(R.id.editText_confirmez);
        editText_mot = (EditText) findViewById(R.id.editText_mot);
        editText_utilisateur = (EditText) findViewById(R.id.editText_utilisateur);

        button_enregistrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!editText_utilisateur.getText().toString().equals("")){
//                    if(sharedPreferences.getString("data","").equals(""))

                    sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
                    sharedPreferences.edit().putString("username",editText_utilisateur.getText().toString()).commit();
                }else{
                    Toast.makeText(SignIn.this, "ENTER A USERNAME", Toast.LENGTH_SHORT).show();
                }

                if(editText_mot.getText().toString().equals(editText_confirmez.getText().toString()))
                {
                    sharedPreferences.edit().putString("password", editText_mot.getText().toString()).commit();
                }
                else
                {
                    Toast.makeText(SignIn.this, "Please enter the same password twice ! ", Toast.LENGTH_SHORT).show();
                }

                Intent intentSignIn = new Intent(SignIn.this,MainActivity.class);
                startActivity(intentSignIn);
                finish();
            }
        });

        textView_signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSignInn = new Intent(SignIn.this,MainActivity.class);
                startActivity(intentSignInn);
                finish();
            }
        });


    }
}