package com.example.examencertificatifsimondelisle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class HighScore extends AppCompatActivity {

    private TextView textView_highScoreUsername, textView_highScoreFinal;

    private Button button_mainMenu;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score);

        button_mainMenu = (Button) findViewById(R.id.button_mainMenu);
        textView_highScoreFinal = (TextView) findViewById(R.id.textView_highScoreFinal);
        textView_highScoreUsername = (TextView) findViewById(R.id.textView_highScoreUsername);


        textView_highScoreUsername.setVisibility(View.GONE);
        textView_highScoreUsername.setVisibility(View.GONE);



        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        String name = sharedPreferences.getString("scoreUser", "error");
        String score = sharedPreferences.getString("score", "error");


        textView_highScoreUsername.setText(String.valueOf(name));
        textView_highScoreUsername.setVisibility(View.VISIBLE);
        textView_highScoreFinal.setText(String.valueOf(score));
        textView_highScoreFinal.setVisibility(View.VISIBLE);

        button_mainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRet = new Intent(HighScore.this,MainActivity.class);
                startActivity(intentRet);
                finish();
            }
        });



    }
}